package com.quickbirdstudios.exerciserxjava.testing_exercise;

import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.example_of_translator.TranslatorViewModel;
import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.example_of_translator.TranslatorViewModelImpl;

import org.junit.Test;

import io.reactivex.observers.TestObserver;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class TranslatorViewModelTest {
    // example of a working unit test
    @Test
    public void testTranslation() {
        // given
        TranslatorViewModel translatorViewModel = new TranslatorViewModelImpl();
        TestObserver<String> testObserver = translatorViewModel.outputGermanText().test();

        // when
        translatorViewModel.inputEnglishText().onNext("Dog");

        // then
        testObserver.assertValueAt(testObserver.valueCount() - 1, "Dog translated");
    }
}
