package com.quickbirdstudios.exerciserxjava.testing_exercise;

import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.LoginViewModel;
import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.LoginViewModelImpl;

import org.junit.Test;

import io.reactivex.observers.TestObserver;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class LoginViewModelTest {
    private LoginViewModel loginViewModel = new LoginViewModelImpl();

    /**
     *  HINT: check the {@link TranslatorViewModelTest} for an example
     */

    @Test
    public void testEmailValidationSucceeds() {
        // TODO 1: implement test for validating that a VALID "inputEmailText" (e.g. "tom@gmail.com")
        // TODO 1: leads to "outputIsEmailValid" being "true"

        fail();
    }

    @Test
    public void testEmailValidationFails() {
        // TODO 2: implement test for validating that an INVALID "inputEmailText" (e.g. "tom")
        // TODO 2: leads to "outputIsEmailValid" being "false"

        fail();
    }

    @Test
    public void testPasswordValidationSucceeds() {
        // TODO 3: implement test for validating that a VALID "inputPasswordText" (e.g. "hallihallo")
        // TODO 3: leads to "outputIsPasswordValid" being "true"

        fail();
    }

    @Test
    public void testPasswordValidationFails() {
        // TODO 4: implement test for validating that an INVALID "inputPasswordText" (e.g. "x")
        // TODO 4: leads to "outputIsPasswordValid" being "false"

        fail();
    }

    @Test
    public void testLoginButtonEnabled_validInput() {
        // TODO 5 (EXTRA): implement test for validating that when both "inputEmailText" and "inputPasswordText" -
        // TODO 5 (EXTRA): have VALID inputs, then the loginButton is enabled

        fail();
    }

    @Test
    public void testLoginButtonEnabled_invalidInput() {
        // TODO 6 (EXTRA): implement test for validating that when either "inputEmailText" or "inputPasswordText" -
        // TODO 6 (EXTRA): have INVALID inputs, then the loginButton is disabled

        fail();
    }
}
