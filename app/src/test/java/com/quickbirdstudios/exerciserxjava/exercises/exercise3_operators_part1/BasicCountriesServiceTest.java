package com.quickbirdstudios.exerciserxjava.exercises.exercise3_operators_part1;

import com.quickbirdstudios.exerciserxjava.providers.CountriesTestProvider;
import com.quickbirdstudios.exerciserxjava.model.Country;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.observers.TestObserver;

public class BasicCountriesServiceTest {

    private BasicCountriesService countriesService;
    private List<Country> allCountries;

    @Rule
    public Timeout globalTimeout = Timeout.seconds(2);

    @Before
    public void setUp() {
        countriesService = new BasicCountriesService();
        allCountries = CountriesTestProvider.countries();
    }

    @Test
    public void rx_CountryNameInCapitals() {
        Country testCountry = CountriesTestProvider.countries().get(0);
        String expected = testCountry.getName().toUpperCase(Locale.US);
        TestObserver<String> testObserver = countriesService
                .countryNameInCapitals(testCountry)
                .test();
        testObserver.assertNoErrors();
        testObserver.assertValue(expected);
    }

    @Test
    public void rx_CountAmountOfCountries() {
        Integer expected = CountriesTestProvider.countries().size();
        TestObserver<Integer> testObserver = countriesService
                .countCountries(allCountries)
                .test();
        testObserver.assertNoErrors();
        testObserver.assertValue(expected);
    }

    @Test
    public void rx_ListPopulationOfEachCountry() {
        List<Long> expectedResult = CountriesTestProvider.populationOfCountries();
        TestObserver<Long> testObserver = countriesService
                .listPopulationOfEachCountry(allCountries)
                .test();
        testObserver.assertValueSet(expectedResult);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_ListNameOfEachCountry() {
        List<String> expectedResult = CountriesTestProvider.namesOfCountries();
        TestObserver<String> testObserver = countriesService
                .listNameOfEachCountry(allCountries)
                .test();
        testObserver.assertValueSet(expectedResult);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_ListOnly3rdAnd4thCountry() {
        List<Country> expectedResult = new ArrayList<>();
        expectedResult.add(allCountries.get(2));
        expectedResult.add(allCountries.get(3));

        TestObserver<Country> testObserver = countriesService
                .listOnly3rdAnd4thCountry(allCountries)
                .test();
        testObserver.assertValueSet(expectedResult);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_IsAllCountriesPopulationMoreThanOneMillion_Positive() {
        TestObserver<Boolean> testObserver = countriesService
                .isAllCountriesPopulationMoreThanOneMillion(CountriesTestProvider.countriesPopulationMoreThanOneMillion())
                .test();
        testObserver.assertResult(true);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_IsAllCountriesPopulationMoreThanOneMillion_Negative() {
        TestObserver<Boolean> testObserver = countriesService
                .isAllCountriesPopulationMoreThanOneMillion(allCountries)
                .test();
        testObserver.assertResult(false);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_ListPopulationMoreThanOneMillion() {
        List<Country> expectedResult = CountriesTestProvider.countriesPopulationMoreThanOneMillion();
        TestObserver<Country> testObserver = countriesService
                .listPopulationMoreThanOneMillion(allCountries)
                .test();
        testObserver.assertValueSet(expectedResult);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_GetCurrencyUsdIfNotFound_When_CountryFound() {
        String countryRequested = "Austria";
        String expectedCurrencyValue = "EUR";
        TestObserver<String> testObserver = countriesService
                .getCurrencyUsdIfNotFound(countryRequested, allCountries)
                .test();
        testObserver.assertResult(expectedCurrencyValue);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_GetCurrencyUsdIfNotFound_When_CountryNotFound() {
        String countryRequested = "Senegal";
        String expectedCurrencyValue = "USD";
        TestObserver<String> testObserver = countriesService
                .getCurrencyUsdIfNotFound(countryRequested, allCountries)
                .test();
        testObserver.assertResult(expectedCurrencyValue);
        testObserver.assertNoErrors();
    }

    @Test
    public void rx_SumPopulationOfCountries() {
        // hint: use "reduce" operator
        TestObserver<Long> testObserver = countriesService
                .sumPopulationOfCountries(allCountries)
                .test();
        testObserver.assertResult(CountriesTestProvider.sumPopulationOfAllCountries());
        testObserver.assertNoErrors();
    }
}
