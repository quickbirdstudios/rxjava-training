package com.quickbirdstudios.exerciserxjava.util;

import java.util.concurrent.TimeUnit;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public final class ExecutionUtils {
    private ExecutionUtils() {
        //no instance
    }

    public static void keepProgramAliveForever() {
        boolean isRunning = true;
        while (isRunning) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
