package com.quickbirdstudios.exerciserxjava.exercises.exercise2_creation_subscription;

import android.annotation.SuppressLint;

import com.quickbirdstudios.exerciserxjava.util.ExecutionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class CreationSubscriptionExercise2 {
    public static void main(String[] args) {
        printEvenNumbersBetween10and100();
    }

    @SuppressLint("CheckResult")
    private static void printEvenNumbersBetween10and100() {
        // TODO 2 print even numbers in the range between 10 and 100
        // example-outpuit: "10,12,14 ..." (make sure to add commas in between)


    }
}
