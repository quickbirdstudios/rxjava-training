package com.quickbirdstudios.exerciserxjava.exercises.exercise0_rxconsole;

import android.annotation.SuppressLint;

import io.reactivex.Observable;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class UserInputExercise {
    private static Observable<String> readConsole() {
        // TODO 1: make all emitted values UPPER CASE
        // TODO 2: filter out all Strings that are shorter than 3 characters
        // TODO 3: show only values that differ from the previous value
        // TODO 3: (HINT: try "distinctUntilChanged")

        // TODO 4 - EXTRA HARD (!): (do this at the end if you still have time and want a challenge)
        // TODO 4 - EXTRA HARD (!): emit every input value 3 times (HINT use "flatMap")

        // chain your operators HERE on the consoleObservable:
        return RxConsole.readLines()/** <--- **/ ;
    }

    @SuppressLint("CheckResult")
    public static void main(String[] args) {
        System.out.println("Good morning, I am RxConsole :-)");
        System.out.println("Type the value you want to emit (type \"exit\" to leave): ");
        System.out.println("You can emit multiple values by seperating them with a \",\"");

        readConsole().subscribe(UserInputExercise::printValue);
        readConsole().subscribe(UserInputExercise::printValue);

    }

    private static void printValue(String value) {
        if(value.isEmpty())System.out.println("Did not receive any value");
        System.out.println("Observed value: " + value);
    }
}
