package com.quickbirdstudios.exerciserxjava.exercises.exercise1_observable_intro;

import io.reactivex.Observable;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class BasicObservableExercises {
/**
 *     !! *** unit tested :-) *** !!
 *     run BasicObservableExercisesTest first to see that all tests FAIL
 *     then make them PASS
 */

//    TODO 0 ! IMPORTANT !: RUN ALL UNIT TESTS IN "test/.../exercises/exercise1_observable_intro"

    /**
     * TODO 1 Return an Observable that emits a single value "Hello World"
     *
     * @return "Hello World!"
     */
    public Observable<String> exerciseHello() {
        return null;
    }

    /**
     * TODO 2 Transform the incoming Observable from "Hello" to "Hello [Name]" where [Name] is your name.
     *
     * @param "Hello Name!"
     */
    public Observable<String> exerciseMap(Observable<String> hello) {
        return null;
    }

    /**
     * TODO 3 Given a stream of numbers, choose the even ones and return a stream like:
     * <p>
     * 2-Even
     * 4-Even
     * 6-Even
     */
    public Observable<String> exerciseFilterMap(Observable<Integer> nums) {
        return null;
    }
}
