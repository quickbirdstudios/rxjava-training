package com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.quickbirdstudios.exerciserxjava.R;
import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.LoginViewModel;
import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.LoginViewModelImpl;
import com.trello.rxlifecycle2.components.support.RxFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Malte Bucksch on 05/06/2018.
 */
public class LoginFragment extends RxFragment {
    private LoginViewModel loginViewModel;
    private CompositeDisposable disposeBag;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModelImpl.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        disposeBag = new CompositeDisposable();

        EditText email = view.findViewById(R.id.email);
        EditText password = view.findViewById(R.id.password);
        Button loginButton = view.findViewById(R.id.login);
        View progressIndicator = view.findViewById(R.id.loading);

        progressIndicator.setVisibility(View.GONE);

        // supply inputs
        Disposable emailDisposable = RxTextView.textChanges(email)
                .subscribe(text -> loginViewModel.inputEmailText().onNext(text.toString()));

        Disposable passwordDisposable = RxTextView.textChanges(password)
                .subscribe(text -> loginViewModel.inputPasswordText().onNext(text.toString()));

        Disposable loginButtonDisposable = RxView.clicks(loginButton)
                .subscribe(v -> {
                    progressIndicator.setVisibility(View.VISIBLE);
                    loginViewModel.inputLoginTrigger().onNext(true);
                });

        // subscribe to outputs
        Disposable emailValidDisposable = loginViewModel.outputIsEmailValid()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isValid -> email.setError(isValid ? null : "Not valid"));

        Disposable passwordValidDisposable = loginViewModel.outputIsPasswordValid()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isValid -> password.setError(isValid ? null : "Not valid"));

        Disposable buttonEnabledDisposable = loginViewModel.outputIsLoginButtonEnabled()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(loginButton::setEnabled);

        Disposable loggedInDisposable = loginViewModel.outputLoggedInTrigger()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(v -> {
                    progressIndicator.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"Logged in",Toast.LENGTH_SHORT).show();
                });

        disposeBag.addAll(emailDisposable, passwordDisposable, emailValidDisposable,
                passwordValidDisposable, buttonEnabledDisposable, loginButtonDisposable,
                loggedInDisposable);
    }

    @Override
    public void onDestroyView() {
        disposeBag.dispose();

        super.onDestroyView();
    }
}
