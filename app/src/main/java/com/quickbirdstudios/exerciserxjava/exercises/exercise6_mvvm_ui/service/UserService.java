package com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.service;

import io.reactivex.Observable;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public interface UserService {
    Observable<Boolean> login(String email, String password);
    Observable<Boolean> logout();
}
