package com.quickbirdstudios.exerciserxjava.exercises.exercise5_operators_part3;

import android.annotation.SuppressLint;

import com.quickbirdstudios.exerciserxjava.exercises.exercise0_rxconsole.RxConsole;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class Exercise {

    @SuppressLint("CheckResult")
    public static void main(String[] args) {
        // -------------  TODO PART 1 ------------

        /**
         * TODO 0: jump to the {@link RxNetworkClient} class
         * TODO 0: and implement it as an Rx-wrapper for the real NetworkClient
         */
        RxNetworkClient rxNetworkClient = new RxNetworkClient();
        Single<String> input = RxConsole.readLine();

        // -------------  TODO PART 2 ------------

        // run the example to play around and provide input in the console

        /**
         * TODO 1: fetch the nearby coffee places for the input query
         * TODO 1: and print the resulting list of cafe's
         * HINT: use flatMap
         */
        /**
         * TODO 2: if the user does not enter any input after a 5 seconds then exit with
         * TODO 2: a "Shutting down..." message
         * HINT: timeout+onErrorReturnItem
         */
        /**
         * TODO 3 EXTRA: print only the name of the resulting cafe list elements (not the full object)
         * HINT: toObservable+flatMapIterable
         */
        /**
         * TODO 4 EXTRA: Make sure the app is not crashing for an empty query
         * TODO 4 EXTRA: just return an empty coffee place list if there was an erro
         *
         * TODO 5 EXTRA: IF YOU FEEL SUPER AMBITIOUS - return "Empty query, cannot handle that" instead
         *
         */

        // add solution here: (it's one single CHAIN of operators in the end)
        input  // <- apply operators here
                .subscribe(result -> {

        });
    }

}
