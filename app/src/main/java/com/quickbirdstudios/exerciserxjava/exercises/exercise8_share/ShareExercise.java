package com.quickbirdstudios.exerciserxjava.exercises.exercise8_share;

import android.annotation.SuppressLint;

import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.service.UserService;
import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.service.UserServiceImpl;
import com.quickbirdstudios.exerciserxjava.util.ExecutionUtils;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class ShareExercise {
    @SuppressLint("CheckResult")
    public static void main(String[] args) {
        UserService userService = new UserServiceImpl();

        // TODO to fix the bug, you only need to add one operator
        // HINT: it's easy and only 9 letters of code to add (including special chars)
        Observable<Boolean> loginObservable = userService
                .login("tom@jerry.com", "cheese");

        loginObservable.subscribe(debuggingPrintObservable);
        loginObservable.subscribe(status ->
                System.out.println("Welcome! You are now logged in")
        );

        ExecutionUtils.keepProgramAliveForever();
    }

    private static Observer<Boolean> debuggingPrintObservable = new Observer<Boolean>() {
        @Override
        public void onSubscribe(Disposable d) {
            System.out.println("onSubscribe");
        }

        @Override
        public void onNext(Boolean s) {
            System.out.println("onNext");
        }

        @Override
        public void onError(Throwable e) {
            System.out.println("onError");
        }

        @Override
        public void onComplete() {
            System.out.println("onComplete");
        }
    };
}
