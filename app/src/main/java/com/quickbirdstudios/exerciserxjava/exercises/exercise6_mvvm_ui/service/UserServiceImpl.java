package com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.service;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class UserServiceImpl implements UserService {
    private static final String TAG = "UserServiceImpl";

    public Observable<Boolean> logout() {
        // mocked
        return Observable.just(true)
                .doOnNext(v -> System.out.println("Logging in..."))
                .delay(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io());
    }

    public Observable<Boolean> login(String email, String password) {
        // mocked
        return Observable.just(true)
                .doOnNext(v -> System.out.println("Logging in..."))
                .delay(3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io());
    }
}
