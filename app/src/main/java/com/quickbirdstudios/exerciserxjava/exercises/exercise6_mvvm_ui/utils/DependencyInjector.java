package com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.utils;

import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.service.UserService;
import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.service.UserServiceImpl;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public final class DependencyInjector {
    private DependencyInjector() {
        //no instance
    }

    public static UserService injectUserService(){
        return new UserServiceImpl();
    }
}
