package com.quickbirdstudios.exerciserxjava.exercises.exercise7_compose_and_sideeffects;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class ComposeAndSideeffectsExercise {
    public static void main(String[] args) {
        Observable<String> errorObservable = Observable.error(
                new IllegalStateException("STATE JUST WRONG. CANT HANDLE."));
        Observable<String> valuesObservable = Observable.just("A", "ONE", "A", "TWO", "A",
                "ONE", "TWO", "THREE", "FOUR");
        Observable<String> emptyObservable = Observable.empty();

        errorObservable
                .compose(logStates())
                .subscribe(ignoringObserver);
        valuesObservable
                .compose(logStates())
                .subscribe(ignoringObserver);
        emptyObservable
                .compose(logStates())
                .subscribe(ignoringObserver);
    }

    private static <T> ObservableTransformer<T, T> logStates() {
        // TODO 1 implement this transformer: it should simply print out each value of the observable
        // HINT use "do..." functions

        // add solution here
        return null;
    }

    private static Observer<String> ignoringObserver = new Observer<String>() {
        @Override
        public void onSubscribe(Disposable d) {
        }

        @Override
        public void onNext(String s) {
        }

        @Override
        public void onError(Throwable e) {
        }

        @Override
        public void onComplete() {
        }
    };
}
