package com.quickbirdstudios.exerciserxjava.exercises.exercise0_rxconsole;

import java.util.Scanner;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Action;
import io.reactivex.functions.Cancellable;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class RxConsole {
    private RxConsole() {
        //no instance
    }

    public static Single<String> readLine() {
        return Single.create(emitter -> {
            Scanner scanner = new Scanner(System.in);
            String input = readLine(scanner);

            emitter.onSuccess(input);
        });
    }

    public static Observable<String> readLines() {
        Scanner scanner = new Scanner(System.in);

        return Observable.create(emitter -> {
            String input;
            while (!(input = readLine(scanner)).equals("exit")) {
                Observable.fromArray(input.split(","))
                        .blockingForEach(emitter::onNext);
            }

            emitter.onComplete();
        });
    }

    private static String readLine(Scanner scanner) {
        System.out.println("----------------------");
        System.out.print("type value(s) here --> ");

        return scanner.nextLine();
    }
}
